const path = require('path');
const basePath = path.resolve(__dirname, '..');
const buildTools = require(path.resolve(basePath, 'node_modules', 'stuff-build-tools'));

module.exports = buildTools.makeWebpack({
    basePath
});
