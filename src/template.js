export default function (author, title) {
    return `<h2 class="title">${title}</h2><div class="author">by ${author}</div>`;
}
