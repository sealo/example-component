import template from './template';

export default class Article {
    constructor (title, author) {
        this.author = author;
        this.title = title;
    }

    getHTML () {
        return template(this.author, this.title);
    }
};
