import Article from './Article';
import chai from 'chai';

const assert = chai.assert;

let article;
const title = 'title';
const author = 'john doe';

describe('Article', () => {

    beforeEach(() => {
        article = new Article(title, author);
    });

    it('should should instantiate correctly', () => {
        assert.strictEqual(article.title, title);
        assert.strictEqual(article.author, author);
    });
});

